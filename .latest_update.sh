#!/usr/bin/bash

new=false
if [ ! -d $HOME/.cs_local/cs-robotique-setup/.git ]; then	
	rm -rf $HOME/.cs_local/
	mkdir $HOME/.cs_local
	cd $HOME/.cs_local
	git clone https://gitlab-research.centralesupelec.fr/aarsh.thakker/cs-robotique-setup.git
	cd $HOME/.cs_local/cs-robotique-setup
	chmod +x update.sh
	./update.sh
	new=true
fi

if [ "$1" = "clean" ]; then
	rm -rf $HOME/.cs_local/
	mkdir $HOME/.cs_local
	cd $HOME/.cs_local
	git clone https://gitlab-research.centralesupelec.fr/aarsh.thakker/cs-robotique-setup.git
	cd $HOME/.cs_local/cs-robotique-setup
	chmod +x update.sh
	./update.sh
	new=true
fi

if [ "$new" = false ]; then
	cd $HOME/.cs_local/cs-robotique-setup
	git reset --hard	
	git fetch origin
	change_log=`git log main..origin/main`
	echo "$change_log"
	if [[ "$change_log" =~ "update.sh" ]]; then
		git pull
		echo "updating the system"
		chmod +x update.sh
		./update.sh
	else
		git pull
	fi
fi

#xfconf-query --channel xfce4-desktop --property /backdrop/screen0/monitorVirtual1/workspace0/last-image --set $HOME/.cs_local/cs-robotique-setup/images/wp-cs-robotique.png

cp $HOME/.cs_local/cs-robotique-setup/.latest_update.sh /home/centralesupelec/latest_update.sh
chmod +x /home/centralesupelec/latest_update.sh
cp $HOME/.cs_local/cs-robotique-setup/.bash_aliases /home/centralesupelec/.bash_aliases
. /home/centralesupelec/.bashrc

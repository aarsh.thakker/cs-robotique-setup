#!/usr/bin/bash

#apt update
#apt install ros-foxy-rqt* ros-noetic-rqt*
#apt install -y libpython3-dev python3-pip
#pip3 install -U argcomplete
#pip install -U colcon-common-extensions
echo "System updated successfully."
cp $HOME/.cs_local/cs-robotique-setup/.latest_update.sh /home/centralesupelec/latest_update.sh
chmod +x /home/centralesupelec/latest_update.sh

reboot

Setup repo for cs-robotique VM




## Features
- If you open the terminal into pre-built ros workspace, it will automatically recognize the ros version and source the require setup.bash file.
- If you want to re-source the workspace, use commands from workspace folder.
	- ```$ sb1``` (for ros1 workspace) 
	- ```$ sb2``` (for ros2 workspace) 

- If you want to initialize ros and source only setup.bash file from ros installation space you can use 
	- ```$ ros1ws``` (to only source the setup.bash from main ros1 path)    
	- ```$ ros1ws <path-to-ws>``` (to source the ros1 workspace and move to the workspace) 
	- ```$ ros2ws``` (to only source the setup.bash from main ros2 path)    
	- ```$ ros2ws <path-to-ws>``` (to source the ros2 workspace and move to the workspace) 
- ```$ exit_ros``` To exit any of the ros workspace and remove all the ros related variable from current bash prompt. You can jump to other version of ros from the same terminal flawlessly. 

## Demo
Demo shows how to use updated bashrc file to make your workflow better


https://user-images.githubusercontent.com/53620577/138263405-63c63dfe-5e0b-4dce-80a9-9e42b7b36ae5.mp4
